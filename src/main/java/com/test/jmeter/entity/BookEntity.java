package com.test.jmeter.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "books")
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class BookEntity {
    @Id
    private String bookId;
    private String name;
    private String author;
    private Boolean isReady;
}