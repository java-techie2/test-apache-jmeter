package com.test.jmeter.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class BookResponse {
    private String bookId;
    private String name;
    private String author;
    private Boolean isReady;
}
