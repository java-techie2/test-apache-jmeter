package com.test.jmeter.repository;

import com.test.jmeter.entity.BookEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<BookEntity, String> {
    @Query("select b from BookEntity b " +
            "where (:id is null or b.bookId =:id) " +
            "and (:author is null or b.author =:author) " +
            "and (:name is null or b.name =:name) " +
            "and (:isReady is null or b.isReady =:isReady) " +
            "")
    List<BookEntity> getListBook(String id, String author, String name, Boolean isReady);
}
