package com.test.jmeter.service;

import com.test.jmeter.entity.BookEntity;
import com.test.jmeter.repository.BookRepository;
import com.test.jmeter.request.BookRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class BookService {
    @Autowired
    BookRepository bookRepository;

    public BookEntity getBook(String id) {
        return bookRepository.findById(id).get();
    }

    public List<BookEntity> getAllBook(String id, String author, String name, Boolean isReady) {
        return bookRepository.getListBook(id, author, name, isReady);
    }

    public BookEntity saveBook(BookRequest request) {
        BookEntity b = BookEntity.builder()
                .bookId(UUID.randomUUID().toString())
                .author(request.getAuthor())
                .name(request.getName())
                .isReady(true)
                .build();
        return bookRepository.save(b);
    }
}
