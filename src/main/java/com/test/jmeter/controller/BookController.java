package com.test.jmeter.controller;

import com.test.jmeter.entity.BookEntity;
import com.test.jmeter.response.BookResponse;
import com.test.jmeter.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/books")
public class BookController {
    @Autowired
    BookService bookService;
    @GetMapping("/{id}")
    public ResponseEntity<BookResponse> getDetailBook(@PathVariable String id){
        BookEntity b = bookService.getBook(id);
        BookResponse bookResponse = BookResponse.builder()
                .bookId(b.getBookId())
                .author(b.getAuthor())
                .name(b.getName())
                .isReady(b.getIsReady())
                .build();
        return ResponseEntity.ok().body(bookResponse);
    }

    @GetMapping("")
    public ResponseEntity<BookResponse> getAllBook()
}
